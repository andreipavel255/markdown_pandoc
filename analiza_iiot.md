# Aplicatii IoT

- connected lights
    - wired network
    - wireless
- smart fridge
    - wireless
- smart doorlock
    - wireless

# Aplicatii IIoT

- aerospace monitoring
    - wireless
- oil and gas monitoring
    - wireless
    - wired networks
- environmental monitoring
    - wireless
    - wired networks

# Wired network

### Avantaje 
- sansa redusa de pierdere a conexiunii
- securitate
- viteza

### Dezavataje 
- mentenanta
- management ul cablurilor - multe device -uri necesita multe cabluri
- mobilitate

# Wireless
### Avantaje 
- mobilitate datorita absentei firelor
- pret
- instalare 

### Dezavantaje
- viteza
- securitate
- device-urile din apropiere pot interfera si pot cauza pierderea conexiunii sau conexiune redusa

# Probleme IoT
- autonomia bateriei device-urilor
    - durata de viata a bateriei poate creste cu circuite integrate care consuma putin in sleep mode
- atacuri cibernetice
    - informarea consumatorului despre potentialele riscuri
     - crearea unei retele private pentru dispozitivele conectate astfel ele nu vor avea acces la fisiere corupte si nu vor interfera cu alte dispozitive nedorite
- integritatea datelor
    -se poate rezolva folosind un algoritm matematic pentru verificare

# Probleme IIoT
- nevoia de conexiune constanta
    - asigurarea unui sistem care nu pierde date chiar si in cazul unor probleme cu conexiune
- data storage 
    - avem nevoie de un spatiu mare de stocare si viteza rapida de filtrare si procesare a datelor
- securitatea datelor
    - in IIoT coruperea datelor poate cauza pierderi financiare mari 
    - crearea unei retele private
    - aceasta problema se poate rezolva si prin criptarea datelor astfel datele sunt  protejate de atacatori

# Aplicatii comerciale
Aplicatiile comerciale sunt aplicatii care ajuta omul la activitatile pe care le frecventeaza in spatiul public(citirea codului de bare, connected lights in cladirile de birouri). Aceste pot fi folosite de oricine

# Aplicatii industriale
Aplicatiile industriale se folosesc in fabrici si au scopul de a spori eficienta si productivitatea.Spre deosebire de aplicatiile comerciale, acestea nu pot fi folosite de oricine.