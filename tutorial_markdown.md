---
  title : Titlul
  author : Autorul
  date : 24-03-2021
...
# Section 1

##   Subsection 1.1

*text italic*

## Subsection 1.2

**text bold**

# Section 2

## Subsection 2.1

> This is a block quotation.  Block quotations are specified by
> proceeding each line with a > character.  The quotation block
> will be indented.
>
> To have paragraphs in block quotations, separate paragraphs
> with a line containing only the block quotation mark character

## Subsection 2.2

# List

## Bullet lists

* bullet 1
* bullet 2
* bullet 3

## Ordered lists

1. primul
2. al doilea
3. al treilea

## Nested Lists
1. element 1
2. element 2
    * element 2.1
    * element 2.2
    * element 2.3
3. element 3

<!-- -->
1. alta lista
2. alta lista 

# Linie orizontala

Acesta este primul paragraf. Va fi despartit de al doilea
paragraf de o linie orizontala

----

Aceste este al doilea paragraf. Dupa el va fi inca o linie orizontala

----

# Tables

  Column 1    Column 2    Column 3
  ---------  ----------  ---------
  L1C1       L1C2        L1C3
  L2C1       L2C2        L2C3
  ---------  ----------  ---------

# Multiline tables

  Column A    Column B      Column 
                                 C
  ---------  ----------  ---------
  Category 1    High        100.00
  High         95.00
  
  Category 2    High         80.50
  High         82.50
  --------------------------------

# Grid tables

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+

# Pipe tables

| Default | left  | Center | Right  |
|---------|:------|:------:|-------:|
|  L1C1   |  L1C2 |  L1C3  |  L1C4  |
|  L2C1   |  L2C2 |  L2C3  |  L2C4  |

# Math

$y=mx+c$


$\sum{x}$

# Links

## Internal links

Vezi [liste](#List)

Vezi [tabele](#Tables)

## External links

[Tutorial Markdown and Pandoc](https://www.flutterbys.com.au/stats/tut/tut17.3.html)


# Image
![imagine](markdown.png)

# Footnote

Acesta este primul footnote[^1]. Acesta este al doilea footnote[^2].

[^1]: primul footnote
[^2]: al doilea footnote
